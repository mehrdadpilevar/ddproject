#ifndef SOFFT_H
#define SOFFT_H

#include <QObject>
#include <QtCharts>
#include <QtQml>

class sofft : public QObject {
  Q_OBJECT
  QString m_softchart;
  int cc = 0;

public:
  explicit sofft(QObject *parent = nullptr);

  Q_PROPERTY(QString softchart READ softchart WRITE setSoftchart NOTIFY
                 softchartChanged)

  QString softchart() const;

public slots:
  void setSoftchart(QString softchart);
  void dataresive();
  void addSeries();
  QString getqml(QVector<QPoint> data);

signals:
  void softchartChanged(QString softchart);
};

#endif // SOFFT_H
