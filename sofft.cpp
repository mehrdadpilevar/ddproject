#include "sofft.h"
#include <qdebug.h>

sofft::sofft(QObject *parent) : QObject(parent) {

  QTimer *timer = new QTimer(this);
  timer->setInterval(100);
  timer->start();
  connect(timer, &QTimer::timeout, this, [=]() -> void {
    dataresive();
    //    timer->stop();
  });
}

QString sofft::softchart() const { return m_softchart; }

void sofft::setSoftchart(QString softchart) {
  if (m_softchart == softchart)
    return;

  m_softchart = softchart;

  emit softchartChanged(m_softchart);
}

void sofft::dataresive() { addSeries(); }
void sofft::addSeries() {
  QVector<QPoint> data;
  for (int i = 0; i < 20; i++) {
    data.append(QPoint(i, rand() % 3));
  }

  QString out = getqml(data);
  setSoftchart(out);
  qDebug() << cc++;
}

QString sofft::getqml(QVector<QPoint> data) {
  QString qml_string =
      "import QtQuick 2.12;import QtQuick.Window 2.12;import QtCharts 2.15; ";
  //  for (int i = 0; i < data.count(); i++) {
  qml_string += "SplineSeries{ color: \"#0000ff\"; useOpenGL: true;";
  for (int j = 0; j < data.count(); j++) {
    qml_string += "XYPoint{x:" + QString::number(data[j].x()) +
                  "; y:" + QString::number(data[j].y()) + " }";
  }
  //    qml_string += "}";
  //  }
  qml_string += "}";
  return qml_string;
}
